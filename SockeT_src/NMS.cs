﻿/* This file is a part of SockeT and licenced under the MIT licence
 * For more informations, see LICENCE or choosealicense.com/licenses/mit/
 */

#if NMS
using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;

namespace SockeT
{
    public static class NMS
    {
        public static bool TimeoutChecks = false;

        public static Action<Socket, string> Sender = (client, file) =>
        {
            while (client.Connected)
            {
                if (File.Exists(file))
                {
                    var fs = new FileStream(file, FileMode.Open, FileAccess.Read);

                    var buffer = new byte[fs.Length + 2];

                    short size = (short)fs.Length;

                    #if (DEBUG && VERBOSE)
                    Debug.Write("[NMS] (in {1} of {2}) Send {0} bytes through NMS to the server.", size, file, client.RemoteEndPoint.ToString());
                    #endif

                    byte[] size_bytes = BitConverter.GetBytes(size);

                    buffer[0] = size_bytes[0];
                    buffer[1] = size_bytes[1];

                    for (int i = 2; fs.Position != fs.Length; i++)
                        buffer[i] = (byte)fs.ReadByte();

                    fs.Close();
                    File.Delete(file);
                    try { client.Send(buffer); } catch { }
                }
                else
                {
                    #if (DEBUG && VERBOSE)
                    Debug.Write("[NMS] (in {0} of {1}) No data to send, send an timeout.", file, client.RemoteEndPoint.ToString());
                    #endif
                    try { client.Send(new byte[2] { 0, 0 }); } catch { }
                }
                Thread.Sleep(Program.WaitDelay);
            }
        };

        public static Action<Socket, string> Reciever = (client, file) =>
        {
            var ns = new NetworkStream(client);

            while (client.Connected)
            {
                if (!File.Exists(file) && ns.DataAvailable)
                {
                    var size_buffer = new byte[2];
                    ns.Read(size_buffer, 0, 2);
                    short size = BitConverter.ToInt16(size_buffer,0);

                    if (size == 0)
                    {
                        #if (DEBUG && VERBOSE)
                        Debug.Write("[NMS] (in {0} of {1}) Recieved an timeout message.", file, client.RemoteEndPoint.ToString());
                        #endif
                        continue;
                    }

                    #if (DEBUG && VERBOSE)
                    Debug.Write("[NMS] (in {0} of {1}) Recieved {0} bytes through the NMS.", file, client.RemoteEndPoint.ToString());
                    #endif

                    var fs = new FileStream(file, FileMode.Create);

                    var buffer = new byte[size];
                    ns.Read(buffer, 0, size);

                    fs.Write(buffer, 0, size);
                    fs.Close();
                }
                Thread.Sleep(Program.WaitDelay);
            }
        };
    }
}
#endif