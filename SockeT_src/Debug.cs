﻿/* This file is a part of SockeT and licenced under the MIT licence
 * For more informations, see LICENCE or choosealicense.com/licenses/mit/
 */

#if DEBUG
using System;
using System.IO;

namespace SockeT
{
    public static class Debug
    {
        public static void Write(string format, params object[] parameters)
        {
            do
            {
                try
                {
                    var fs = new FileStream("SockeT.log", FileMode.Append);
                    var tw = new StreamWriter(fs);


                    if (fs == null || !fs.CanWrite)
                        return;

                    string line = string.Format(format, parameters);
                    line = string.Concat("[", DateTime.Now.ToString(), "] ", line);

                    Console.Error.WriteLine(line);
                    tw.WriteLine(line);

                    // Flush textwritter
                    tw.Flush();

                    // Flush & Close filestream
                    fs.Flush(); fs.Close();
                } catch { continue; }
            // Can look stupid but avoid error when multiples call are done at the same time.
            } while (false);

        }
    }
}

#endif
