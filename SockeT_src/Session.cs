﻿/* This file is a part of SockeT and licenced under the MIT licence
* For more informations, see LICENCE or choosealicense.com/licenses/mit/
*/

#if SERVER
using System;
using System.Net.Sockets;
using System.Threading;
using System.IO;

namespace SockeT
{
    public class Session : IDisposable
    {
        public Session(Socket socket, Thread checker, Thread sender, Thread reciever, string file)
        {
            Socket = socket;
            Checker = checker;
            Sender = sender;
            Reciever = reciever;
            this.file = file;
        }

        public Socket Socket;
        public Thread Checker, Sender, Reciever;
        public string file;

        public void Dispose()
        {
            #if DEBUG
            Debug.Write("Session {0} disposed", Socket.RemoteEndPoint.ToString());
            #endif

            Sender.Abort();
            Reciever.Abort();
            Socket.Disconnect(false);
            File.Delete(file);
            
			while (true) 
				try 
				{
					File.Delete(string.Concat(file, "_OUT"));
					break;
				} catch { Thread.Sleep(Program.WaitDelay); }

            Checker.Abort();
        }
    }
}
#endif