﻿/* This file is a part of SockeT and licenced under the MIT licence
 * For more informations, see LICENCE or choosealicense.com/licenses/mit/
 */

#if TCP
using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;

namespace SockeT
{
    public static class TCP
    {
        public static Action<Socket, string> Sender = (client, file) =>
        {
            while (client.Connected)
            {
                if (File.Exists(file))
                {
                    #if (DEBUG && VERBOSE)
                    Debug.Write("[TCP] (in {0} of {1}) Try to send datas.", file, client.RemoteEndPoint.ToString());
                    #endif

                    FileStream fs;
                    try
                    {
                        fs = new FileStream(file, FileMode.Open, FileAccess.Read);
                    } catch { continue; }


                    while (fs.Position != fs.Length)
                    {
                        int count = (int)Math.Min(8192, fs.Length - fs.Position);
                        var buffer = new byte[count];
                        fs.Read(buffer, 0, count);
                        try { client.Send(buffer); } catch { break; }
                    }

                    fs.Close();
                    File.Delete(file);
                }
                Thread.Sleep(Program.WaitDelay);
            }
        };
        public static Action<Socket, string> Reciever = (client, file) =>
        {
            while (client.Connected)
            {
                if (client.Available > 0 && !File.Exists(file))
                {
                    #if (DEBUG && VERBOSE)
                    Debug.Write("[TCP] (in {0} of {1}) Recieve datas.", file, client.RemoteEndPoint.ToString());
                    #endif

                    FileStream fs;
                    try
                    {
                        fs = new FileStream(file, FileMode.CreateNew, FileAccess.Write);
                    } catch { continue; }

                    var buffer = new byte[8192];

                    while (true)
                    {
                        while (client.Available > 0)
                        {
                            int count = client.Receive(buffer);
                            fs.Write(buffer, 0, count);
                        }

						#if !TCP_RECIEVE_NOSLEEP
						Thread.Sleep(100);
						#endif
						
                        if (client.Available == 0)
                            break;
                    }

                    fs.Flush();
                    fs.Close();
                }
                Thread.Sleep(Program.WaitDelay);
            }
        };
    }
}
#endif
