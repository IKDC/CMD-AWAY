﻿/* This file is a part of SockeT and licenced under the MIT licence
 * For more informations, see LICENCE or choosealicense.com/licenses/mit/
 */

 namespace SockeT
{
    public interface Module
    {
        void Init();
        void Start();
        void Stop();
        bool Active();
    }
}
